require 	'rails_helper'

describe EventJobPeriodAssociation do
  before do
    @event_job_period_association= EventJobPeriodAssociation.new(event_id:"1", job_id:"1")
  end

  subject { @event_job_period_association }

  it {should respond_to(:event_id) }
  it {should respond_to(:job_id) }

  it {should be_valid }

  describe "when event_id is not present" do
    before {@event_job_period_association.event_id = "" }
    it {should_not be_valid }
  end

  describe "when job_id is not present" do
    before {@event_job_period_association.job_id = "" }
    it {should_not be_valid }
  end

end
