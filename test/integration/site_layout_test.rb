require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  test "layout links" do
    get root_path
    get signup_path
    get login_path
    assert_template 'sessions/new'
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", offer_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", signup_path
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href=?]", impressum_path
    assert_select "a[href=?]", impression_path
  end

end